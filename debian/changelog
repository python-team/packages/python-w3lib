python-w3lib (2.3.1-1) unstable; urgency=medium

  * New upstream release.

 -- Andrey Rakhmatullin <wrar@debian.org>  Thu, 30 Jan 2025 00:02:15 +0500

python-w3lib (2.2.1-1) unstable; urgency=medium

  * New upstream release.

 -- Andrey Rakhmatullin <wrar@debian.org>  Sat, 15 Jun 2024 00:33:45 +0500

python-w3lib (2.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Add B-D: python3-sphinx-rtd-theme.

 -- Andrey Rakhmatullin <wrar@debian.org>  Thu, 06 Jun 2024 00:14:13 +0500

python-w3lib (2.1.2-3) unstable; urgency=medium

  * Build with python-scrapy-doc.
  * Fix the tox object.inv path.

 -- Andrey Rakhmatullin <wrar@debian.org>  Wed, 24 Apr 2024 11:58:43 +0500

python-w3lib (2.1.2-2) unstable; urgency=medium

  * Add the python-w3lib-doc subpackage.
  * Bump Standards-Version to 4.7.0.

 -- Andrey Rakhmatullin <wrar@debian.org>  Mon, 22 Apr 2024 22:25:57 +0500

python-w3lib (2.1.2-1.1) unstable; urgency=medium

  * Team Upload
  * Remove obsolete dependency on python3-six

 -- Alexandre Detiste <tchet@debian.org>  Tue, 16 Jan 2024 23:42:23 +0100

python-w3lib (2.1.2-1) unstable; urgency=medium

  * New upstream release.
    + Fix tests on Python 3.11.4+ (Closes: #1042345).
  * Switch to autopkgtest-pkg-pybuild.

 -- Andrey Rakhmatullin <wrar@debian.org>  Fri, 04 Aug 2023 23:46:03 +0400

python-w3lib (2.1.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.2.

 -- Andrey Rakhmatullin <wrar@debian.org>  Wed, 01 Feb 2023 00:56:39 +0400

python-w3lib (2.0.1-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + python3-w3lib: Drop versioned constraint on python3-six in Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Set upstream metadata fields: Repository.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 22:03:24 +0100

python-w3lib (2.0.1-1) unstable; urgency=medium

  * New upstream release.
  * Run build time tests with pytest.
  * Set the Homepage field to the Github page.
  * Replace py3versions -r with py3versions -s in the autopkgtest.
  * Bump Standards-Version to 4.6.1.

 -- Andrey Rahmatullin <wrar@debian.org>  Sun, 18 Sep 2022 20:50:08 +0500

python-w3lib (1.22.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Andrey Rahmatullin ]
  * Remove the test for the semicolon separator handling, that fails with
    the recent Python security update (Closes: #984492).

 -- Andrey Rahmatullin <wrar@debian.org>  Sun, 07 Mar 2021 22:51:14 +0500

python-w3lib (1.22.0-2) unstable; urgency=medium

  * Add autopkgtests.

 -- Andrey Rahmatullin <wrar@debian.org>  Sun, 09 Aug 2020 14:29:38 +0500

python-w3lib (1.22.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.5.0.
  * Bump the debhelper compat level to 13.
  * Move enabling the python3 addon from rules to B-D.

 -- Andrey Rahmatullin <wrar@debian.org>  Sun, 19 Jul 2020 21:59:44 +0500

python-w3lib (1.21.0-1) unstable; urgency=medium

  * New upstream version.

 -- Andrey Rahmatullin <wrar@debian.org>  Sat, 10 Aug 2019 21:18:43 +0500

python-w3lib (1.20.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Andrey Rahmatullin ]
  * Add myself to Uploaders.
  * Drop Python 2 support.
  * Bump the debhelper compat level to 12.
  * Add Rules-Requires-Root: no.

 -- Andrey Rahmatullin <wrar@debian.org>  Fri, 02 Aug 2019 23:53:08 +0500

python-w3lib (1.20.0-1) unstable; urgency=low

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

  [ Michael Fladischer ]
  * New upstream release.
  * Bump Standards-Version to 4.3.0.

 -- Michael Fladischer <fladi@debian.org>  Fri, 01 Feb 2019 12:54:46 +0100

python-w3lib (1.19.0-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Always use pristine-tar.
  * Bump debhelper compatibility and version to 11.
  * Bump Standards-Version to 4.1.3.
  * Enable autopkgtest-pkg-python testsuite.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Use https:// for uscan URL.

 -- Michael Fladischer <fladi@debian.org>  Sat, 27 Jan 2018 13:43:00 +0100

python-w3lib (1.18.0-1) unstable; urgency=low

  * Team upload.
  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Tue, 08 Aug 2017 12:47:23 +0200

python-w3lib (1.17.0-1) unstable; urgency=low

  * Team upload.
  * Upload to unstable.
  * Bump Standards-Version to 4.0.0.

 -- Michael Fladischer <fladi@debian.org>  Tue, 20 Jun 2017 10:21:14 +0200

python-w3lib (1.17.0-1~exp1) experimental; urgency=low

  * Team upload.
  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Fri, 17 Feb 2017 09:15:40 +0100

python-w3lib (1.16.0-1~exp1) experimental; urgency=low

  [ Michael Fladischer ]
  * Team upload.
  * New upstream release.
  * Clean files in w3lib.egg-info/ to allow two builds in a row.
  * Add python(3)-six to Build-Depends so tests can run.
  * Bump Standards-Version to 3.9.8.
  * Use https:// for copyright-format 1.0 URL.
  * Remove version requirement from python-all for Build-Depends.
  * Reformat packaging files with cme for better readability.
  * Fix spelling error in long description.
  * Restructure d/copyright to remove duplicate license texts and rename
    to BSD-3-clause.
  * Remove duplicate words from short description.

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)

 -- Michael Fladischer <fladi@debian.org>  Tue, 24 Jan 2017 16:35:02 +0100

python-w3lib (1.11.0-1) unstable; urgency=medium

  * Team upload
  * Fresh release (Closes: #757842)
  * debian/watch
    - use pypi.debian.net redirector
  * debian/control
    - Versioned depends on python-six (>= 1.6.1) (Closes: #749496)
    - boosted policy to 3.9.6

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 24 Jun 2015 16:51:18 -0400

python-w3lib (1.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream release. (Closes: #734545)
  * Add Vcs-* fields in debian/control. (Closes: #735306)
  * Create python3 package using pybuild and dh_python3 (merged from Ubuntu).
  * Update Standards version to 3.9.5.

 -- Vincent Cheng <vcheng@debian.org>  Tue, 01 Apr 2014 12:58:22 -0700

python-w3lib (1.0-1) unstable; urgency=low

  * Initial release. (Closes: #659315)

 -- Ignace Mouzannar <mouzannar@gmail.com>  Fri, 10 Feb 2012 07:12:01 +0400
